FROM golang:latest
LABEL authors="arkadiusz.tulodziecki@mobica.com"

RUN go install honnef.co/go/tools/cmd/staticcheck@latest
RUN go install github.com/qiniu/checkstyle/gocheckstyle@latest

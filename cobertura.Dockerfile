FROM golang:1.20
LABEL authors="arkadiusz.tulodziecki@mobica.com"

# our core requirement
RUN go install github.com/boumenot/gocover-cobertura@latest

# our E2E tests uses python based project
RUN apt-get update && apt-get install -y \
  git python3 python3-setuptools python3-pip python3-venv procps net-tools \
  && rm -rf /var/lib/apt/lists/* \
